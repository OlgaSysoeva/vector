﻿using System;

namespace Vector
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector a = new Vector(2,4);
            Vector b = new Vector(5,2);
            
            Console.WriteLine($"Вектор А({a.x};{a.y})");
            Console.WriteLine($"Вектор B({b.x};{b.y})");

            Console.WriteLine("Операции над векторами:");

            Vector c = a + b;
            Console.WriteLine($"{a}+{b}={c}");

            Vector d = a - b;
            Console.WriteLine($"{a}-{b}={d}");

            double k = 8;
            Vector f = k * a;
            Console.WriteLine($"{k}*{a}={f}");

            Vector g = b * k;
            Console.WriteLine($"{b}*{k}={g}");

            double j = a * b;
            Console.WriteLine($"{a}*{b}={j}");

            Console.WriteLine($"|{a}|={!a}");

            Console.WriteLine("Обращение по индексу:");
            Console.WriteLine($"A[0]={a[0]}");
            Console.WriteLine($"B[1]={b[1]}");

            // Делаем вектор нормальным (единичным вектором)
            Vector n = a / !a;
            Console.WriteLine($"Проверяем является ли вектор ({n}) нормалью:");
            if (n)
                Console.WriteLine("Вектор является нормалью.");
            else
                Console.WriteLine("Вектор не является нормалью.");

            Console.ReadKey();
        }
    }
}
