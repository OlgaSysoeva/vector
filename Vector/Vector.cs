﻿using System;

namespace Vector
{
    public class Vector
    {
        public double x;
        public double y;

        public Vector(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return $"({x},{y})";
        }

        public double this[int number]
        {
            get
            {
                if (number == 0) return x;
                if (number == 1) return y;
                throw new IndexOutOfRangeException();
            }
        }

        public static Vector operator +(Vector a, Vector b)
            => new Vector(a.x + b.x, a.y + b.y);

        public static Vector operator -(Vector a, Vector b)
            => new Vector(a.x - b.x, a.y - b.y);

        public static Vector operator *(double k, Vector a)
            => new Vector(a.x * k, a.y * k);

        public static Vector operator *(Vector a, double k)
            => k * a;

        public static double operator *(Vector a, Vector b)
            => a.x * b.x + a.y * b.y;

        public static double operator !(Vector a)
            => Math.Sqrt(a * a);

        public static bool operator true(Vector a)
            => Math.Abs(a * a - 1) < 1E-12;

        public static bool operator false(Vector a)
            => a ? false : true;

        public static Vector operator /(Vector a, double k)
            => (1/k) * a;
    }
}
